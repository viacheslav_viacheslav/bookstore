FROM goyalzz/ubuntu-java-8-maven-docker-image
ADD /target/bookstore-*.jar /
EXPOSE 8080
ENTRYPOINT java -jar bookstore-*.jar
