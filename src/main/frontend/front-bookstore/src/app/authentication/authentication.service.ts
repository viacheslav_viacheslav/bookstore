import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AuthenticationService {

  public API = 'http://localhost:8081';
  public SLASH = '/';
  public GENERATE_TOKEN = 'token/generate-token';

  constructor(private http: HttpClient) {
  }

  attemptAuth(ussername: string, password: string): Observable<any> {
    const credentials = {username: ussername, password: password};
    console.log('attempAuth ::');
    return this.http.post(this.API+this.SLASH+this.GENERATE_TOKEN, credentials);
  }

}
