import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpUserEvent, HttpErrorResponse, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {TokenStorageService} from '../token-storage/token-storage.service';
import 'rxjs/add/operator/do';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(private token: TokenStorageService, private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('interception');
    let authReq = req;
    if (this.token.getToken() != null) {
      authReq = req.clone({headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + this.token.getToken())});
      console.log('token from interceptor: ');
      console.log(TOKEN_HEADER_KEY, 'Bearer ' + this.token.getToken());
    }
    return next.handle(authReq).do(
      () => {},
      (err: any) => {
        console.log("HANDLING")
        if (err instanceof HttpErrorResponse) {
          console.log("HANDLING ERROR: "+ err.status);
          if (err.status === 401) {
            this.router.navigate(['login']);
          }
        }
      }
    );
  }

}
