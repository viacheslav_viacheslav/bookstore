import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Book} from './book';
import {DomSanitizer} from '@angular/platform-browser';
import {BookServiceService} from '../books-page/book-service.service';
import {OrdersService} from '../orders-page/orders.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  private bookFilesUrl = this.bookService.baseUrl + '/books/files/';

  @Input() book: Book;
  @Output() orderBook: EventEmitter<String> = new EventEmitter<String>();

  addBookToOrder(event, bookId: number) {
    this.orderBook.emit(bookId.toString());
  }


  constructor(private sanitizer: DomSanitizer, private bookService: BookServiceService, private orderService: OrdersService) {
  }

  ngOnInit() {
    this.book.photoPathSecure = this.sanitizer.bypassSecurityTrustResourceUrl(this.bookFilesUrl + this.book.photoPath);
  }


}
