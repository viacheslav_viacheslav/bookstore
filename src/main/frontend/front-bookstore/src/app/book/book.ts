export class Book {
  bookId: number;
  title:string;
  price:number;
  quantity: number;
  author:string;
  photoPath: string;
  visible: Boolean = false;
  photoPathSecure;
  file;

}
