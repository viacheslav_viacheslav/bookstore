import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../token-storage/token-storage.service';
import {Router} from '@angular/router';
import {AuthGuard} from '../authGuard/auth-guard.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private tokenStorage: TokenStorageService,
              private router: Router,
              public authGuardService: AuthGuard) { }

  ngOnInit() {
  }

  public logout(){
    this.tokenStorage.signOut();
    this.router.navigate(['login']);
  }
}
