import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {
  public API = '//localhost:8081';
  public USER_API = this.API + '/users';




  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    return this.http.get(this.USER_API);
  }

  get(id: string){
    return this.http.get(this.USER_API+'/'+ id);
  }

  save(user: any): Observable<any> {
    let result: Observable<Object>;
    if (user['href']) {
      // result = this.http.put(user.href, user);
    } else {
      result = this.http.post(this.USER_API+'/save', user);
    }
    return result;
  }

  getUsersByInput(input: string, page:number, pageSize: number, sortCriteria: string, sortOrder: string){
    console.log(this.API+'/users/search/findByInput'+'?'+'login='+input+'&'+'page='+page+'&'+'size='+pageSize+'&'+'sort='+sortCriteria+','+sortOrder);
    return this.http.get(this.API+'/users/search/findByInput'+'?'+'login='+input+'&'+'page='+page+'&'+'size='+pageSize+'&'+'sort='+sortCriteria+','+sortOrder);
  }

  deleteUser(id: number){
    console.log(this.API+'/users/'+id);
    return this.http.delete(this.API+'/users/'+id);
  }

  findByLogin(login: string){
    console.log(this.API+'/users/search/findByLogin' + '?'+'login='+login);
    return this.http.get(this.API+'/users/search/findByLogin' + '?'+'login='+login);
  }

}


