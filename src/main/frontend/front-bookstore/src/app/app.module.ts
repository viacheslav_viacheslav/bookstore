import {BrowserModule} from '@angular/platform-browser';
import {UserService} from './user.service';
import {NgModule} from '@angular/core';
import {FormBuilder, FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';


import {AppComponent} from './app.component';
import {LoginFormComponent} from './login-form/login-form.component';
import {RegistrationFormComponent} from './registration-form/registration-form.component';
import {RouterModule, Routes} from '@angular/router';
import {HeaderComponent} from './header/header.component';
import {CommonModule} from '@angular/common';
import {
  MatDialogModule, MatToolbarModule, MatInputModule, MatTableModule, MatPaginatorModule,
  MatProgressSpinnerModule, MatSortModule, MatTooltip
} from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSidenavModule} from '@angular/material/sidenav';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthenticationService} from './authentication/authentication.service';
import {TokenStorageService} from './token-storage/token-storage.service';
import {Interceptor} from './interceptor/interceptor.service';
import {AuthGuard} from './authGuard/auth-guard.service';
import {BooksPageComponent} from './books-page/books-page.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BookComponent} from './book/book.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {LeftMenuComponent} from './left-menu/left-menu.component';
import {GeneralLayoutComponent} from './general-layout/general-layout.component';
import {BookServiceService} from './books-page/book-service.service';
import {BookAddComponent} from './book-add/book-add.component';
import {OrdersPageComponent} from './orders-page/orders-page.component';
import {OrdersService} from './orders-page/orders.service';
import {UsersPageComponent} from './users-page/users-page.component';
import {BooksEditPageComponent} from './books-edit-page/books-edit-page.component';
import {BookIdTransferService} from './books-edit-page/book-id-transfer.service';
import { AdminHomeComponent } from './admin-home/admin-home.component';


const appRoutes: Routes = [
  {
    path: 'registration', component: RegistrationFormComponent
    , data: {expectedRoles: []}
  },
  {path: 'login', component: LoginFormComponent, data: {expectedRoles: []}},
  {path: '', redirectTo: 'login', pathMatch: 'full', canActivate: [AuthGuard], data: {expectedRoles: []}},
  // {path: 'books', component: BooksPageComponent},
  {
    path: 'layout', component: GeneralLayoutComponent
    , children: [{path: 'books', component: BooksPageComponent}], canActivate: [AuthGuard], data: {expectedRoles: ['ROLE_CUSTOMER']}
  },
  {
    path: 'layout',
    component: GeneralLayoutComponent,
    children: [{path: 'book-add', component: BookAddComponent}],
    canActivate: [AuthGuard],
    data: {expectedRoles: ['ROLE_ADMINISTRATOR']}
  },
  {
    path: 'layout',
    component: GeneralLayoutComponent,
    children: [{path: 'orders', component: OrdersPageComponent}],
    canActivate: [AuthGuard],
    data: {expectedRoles: ['ROLE_CUSTOMER']}
  },
  {
    path: 'layout',
    component: GeneralLayoutComponent,
    children: [{path: 'registration', component: RegistrationFormComponent}],
    canActivate: [AuthGuard],
    data: {expectedRoles: ['ROLE_ADMINISTRATOR','ROLE_CUSTOMER']}
  },
  {
    path: 'layout',
    component: GeneralLayoutComponent,
    children: [{path: 'users', component: UsersPageComponent}],
    canActivate: [AuthGuard],
    data: {expectedRoles: ['ROLE_ADMINISTRATOR']}
  },
  {
    path: 'layout',
    component: GeneralLayoutComponent,
    children: [{path: 'booksEdit', component: BooksEditPageComponent}],
    canActivate: [AuthGuard],
    data: {expectedRoles: ['ROLE_ADMINISTRATOR']}
  },
  {
    path: 'layout',
    component: GeneralLayoutComponent,
    children: [{path: 'bookEdit', component: BookAddComponent}],
    data: {expectedRoles: []}
  },
  {
    path: 'layout',
    component: GeneralLayoutComponent,
    children: [{path: 'admin-home', component: AdminHomeComponent}],
    canActivate: [AuthGuard],
    data: {expectedRoles: ['ROLE_ADMINISTRATOR']}
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    RegistrationFormComponent,
    HeaderComponent,
    BooksPageComponent,
    BookComponent,
    LeftMenuComponent,
    GeneralLayoutComponent,
    BookAddComponent,
    OrdersPageComponent,
    UsersPageComponent,
    BooksEditPageComponent,
    AdminHomeComponent
    // PasswordValidationDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    MatToolbarModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatDialogModule,
    FlexLayoutModule,
    NgxPaginationModule,
    MatInputModule,
    MatTableModule,
    CommonModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTooltipModule,
  ],
  providers: [UserService, FormBuilder, AuthenticationService, TokenStorageService, AuthGuard, BookServiceService, OrdersService, BookIdTransferService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
