import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {AuthenticationService} from '../authentication/authentication.service';
import {TokenStorageService} from '../token-storage/token-storage.service';
import {AuthGuard} from '../authGuard/auth-guard.service';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  errors = [];

  ngOnInit() {
  }


  constructor(private router: Router,
              public dialog: MatDialog,
              private authService: AuthenticationService,
              private token: TokenStorageService,
              private authGuardService: AuthGuard) {
  }

  username: string;
  password: string;

  login(): void {
    console.log('username: ' + this.username);
    console.log('password: ' + this.password);
    this.authService.attemptAuth(this.username, this.password).subscribe(
      data => {
        this.token.saveToken(data.token);
        if (this.authGuardService.getCurrentUserRole() === 'ROLE_ADMINISTRATOR') {
          this.router.navigate(['layout/admin-home']);
        } else {
          this.router.navigate(['layout/books']);
        }
      }, error => {
        this.errors.push(error);
      }
    );
  }

}
