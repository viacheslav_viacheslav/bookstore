import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BookServiceService} from '../books-page/book-service.service';
import {Book} from '../book/book';
import {BookIdTransferService} from '../books-edit-page/book-id-transfer.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Location} from '@angular/common';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.css']
})
export class BookAddComponent implements OnInit,OnDestroy{

  book: Book=new Book();
  selectedFile: File = null;
  bookIdToEdit: number;
  idSubscribtion;
  private bookFilesUrl = this.bookService.baseUrl+"/books/files/"

  @ViewChild('fileInput')
  fileInputVar;

  constructor(private sanitizer: DomSanitizer,
              private bookService: BookServiceService,
              private idService: BookIdTransferService,
              private location: Location) {
  }

  ngOnInit(){
   this.idSubscribtion= this.idService.currentId.subscribe(bookId=>this.bookIdToEdit=bookId);
    console.log("book id "+this.bookIdToEdit);


    if(this.bookIdToEdit!==null && this.bookIdToEdit !==0){
      this.bookService.getBook(this.bookIdToEdit).subscribe(data => {
        this.book=<Book>data;
        this.book.photoPathSecure = this.sanitizer.bypassSecurityTrustResourceUrl(this.bookFilesUrl + this.book.photoPath);
        console.log(this.bookFilesUrl + this.book.photoPath);
        console.log(data);
      })
    }
  }

  submit() {
    let formData = this.createFormData();
    if(this.bookIdToEdit==null || this.bookIdToEdit ==0) {
      console.log("book POST");
      this.bookService.createBook(formData).subscribe(data => {
          this.book = new Book();
          this.fileInputVar.nativeElement.value = '';
          alert('Book added');
        },
        error => {
          console.log(error.message);
        });
    }else{

      const formData2 = this.createFormData();
      this.bookService.updateBook(formData2).subscribe(data => {
          this.book = new Book();
          this.fileInputVar.nativeElement.value = '';
          alert('Book changed');
        },
        error => {
          console.log(error.message);
        });
    }


  }

  onSelectFile(event) {
    this.selectedFile = event.target.files[0];
    var reader = new FileReader();

    reader.onload = (event:any) => {
      this.book.photoPathSecure = event.target.result;
    }

    reader.readAsDataURL(event.target.files[0]);
  }

  createFormData(): FormData{
    const formData = new FormData();
    if(this.bookIdToEdit!==null && this.bookIdToEdit !==0){
      formData.append('bookId',this.bookIdToEdit.toString());
    }
    formData.append('name', this.book.title);
    formData.append('file', this.selectedFile);
    formData.append('price', this.book.price.toString());
    formData.append('quantity', this.book.quantity.toString());
    formData.append('visible', this.book.visible.toString());
    return formData;
  }

  cancel(){
    this.location.back();
  }

  ngOnDestroy(){
    this.idService.changeId(0);
    this.idSubscribtion.unsubscribe();
  }

}
