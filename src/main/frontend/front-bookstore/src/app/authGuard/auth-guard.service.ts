import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Router} from '@angular/router';
import {TokenStorageService} from "../token-storage/token-storage.service";
import * as decode from 'jwt-decode';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private token: TokenStorageService,
              private myRoute: Router){
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const expectedRoles: Array<string> = next.data.expectedRoles;
    const userRole = this.getCurrentUserRole();
    if((userRole != null && expectedRoles.includes(userRole)) || expectedRoles.length==0){
      return true;
    }else{
      this.myRoute.navigate(["login"]);
      return false;
    }
  }

  getCurrentUserRole(){
    const token = this.token.getToken();
    if (!token){
      return;
    }
    return decode(token).role["authority"];
  }

  renderBasedOnRoles(roles: string[]){
    return roles.includes(this.getCurrentUserRole());
  }
   getCurrentUserLogin(){
     const token = this.token.getToken();
     if (!token){
       return;
     }
     console.log("USERNAME: "+decode(token).sub);
     return decode(token).sub;
   }
}
