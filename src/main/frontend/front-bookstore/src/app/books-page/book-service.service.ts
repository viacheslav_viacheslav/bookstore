import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class BookServiceService {
  public baseUrl: string = "http://localhost:8081";

  constructor(private _http:HttpClient) { }

  getVisibleBooks(page:number, pageSize: number){
    return this._http.get(this.baseUrl+'/books/search/visible'+'?'+'page='+page+'&'+'size='+pageSize);
  }

  createBook(data: FormData){
    return this._http.post(this.baseUrl+'/books/add', data);
  }

  updateBook(data: FormData){
    return this._http.post(this.baseUrl+'/books/update', data);
  }

  getBooksByInput(input: string, page:number, pageSize: number, sortCriteria: string, sortOrder: string){
    console.log(this.baseUrl+'/books/search/findByInput'+'?'+'bookTitle='+input+'&'+'page='+page+'&'+'size='+pageSize+'&'+'sort='+sortCriteria+','+sortOrder);
    return this._http.get(this.baseUrl+'/books/search/findByInput'+'?'+'bookTitle='+input+'&'+'page='+page+'&'+'size='+pageSize+'&'+'sort='+sortCriteria+','+sortOrder);
  }

  deleteBook(id: number){
    console.log(this.baseUrl+'/books/'+id);
    return this._http.delete(this.baseUrl+'/books/'+id);
  }

  getBook(id: number){
    console.log(this.baseUrl+'/books/'+id);
    return this._http.get(this.baseUrl+'/books/'+id);
  }
}
