import {Component, OnInit, ViewChild} from '@angular/core';
import {BookServiceService} from './book-service.service';
import {PaginationInstance} from 'ngx-pagination';
import {Book} from '../book/book';
import {OrdersService} from '../orders-page/orders.service';

@Component({
  selector: 'app-books-page',
  templateUrl: './books-page.component.html',
  styleUrls: ['./books-page.component.css']
})
export class BooksPageComponent implements OnInit {

  public directionLinks: boolean = true;
  public books: Array<Book>;

  public config: PaginationInstance = {
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(private bookService: BookServiceService, private orderService: OrdersService) {
  }

  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this.bookService.getVisibleBooks(this.config.currentPage, this.config.itemsPerPage).subscribe(
      data => {
        this.books = data['_embedded'].books;
        this.config.totalItems = data['page'].totalElements;
        console.log(this.books);
      },
      error => {
        console.log(error.error.message);
      }
    );
  }

  onPageChange(page: number) {
    this.config.currentPage = page;
    this.getBooks();
  }

  addBookToOrders(bookId: number) {
    const formData = new FormData();
    formData.append('bookId', bookId.toString());
    this.orderService.createOrder(formData).subscribe(() => this.ngOnInit());
  }
}
