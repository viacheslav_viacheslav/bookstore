import {Book} from '../book/book';

export class Order {
  orderId: number;
  bookTitle: string;
  price: number;
  book: Book;
}
