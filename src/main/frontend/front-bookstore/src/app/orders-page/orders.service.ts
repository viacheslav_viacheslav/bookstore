import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable()
export class OrdersService {
  public baseUrl: string = "http://localhost:8081";

  constructor(private _http:HttpClient) { }

  getOrdersByLoginAndInput(login: string, input: string, page:number, pageSize: number, sortCriteria: string, sortOrder: string){
    console.log(this.baseUrl+'/orderses/search/findForLogin'+'?'+'login='+login+'&'+'bookTitle='+input+'&'+'page='+page+'&'+'size='+pageSize+'&'+'sort='+sortCriteria+','+sortOrder);
    return this._http.get(this.baseUrl+'/orderses/search/findForLogin'+'?'+'login='+login+'&'+'bookTitle='+input+'&'+'page='+page+'&'+'size='+pageSize+'&'+'sort='+sortCriteria+','+sortOrder);
  }

  deleteOrder(id: number){
    console.log(this.baseUrl+'/orderses/'+id);
    return this._http.delete(this.baseUrl+'/orderses/'+id);
  }

  createOrder(formData: FormData){
    console.log(this.baseUrl+'/orderses/add',formData);
    return this._http.post(this.baseUrl+'/orderses/add',formData);
  }
}
