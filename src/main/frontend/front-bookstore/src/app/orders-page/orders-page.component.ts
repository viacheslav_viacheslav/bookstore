import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Order} from './order';
import {OrdersService} from './orders.service';
import {distinctUntilChanged, tap} from 'rxjs/operators';
import {merge} from 'rxjs/observable/merge';
import {fromEvent} from 'rxjs/observable/fromEvent';
import {debounceTime} from 'rxjs/operators';
import {AuthGuard} from '../authGuard/auth-guard.service';


@Component({
  selector: 'app-orders-page',
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.css']
})
export class OrdersPageComponent implements AfterViewInit, OnInit {

  displayedColumns = ['id', 'bookTitle', 'Price', 'Date', 'delete'];
  private orders: Array<Order>;
  public dataSource;
  private currentUserName;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  constructor(private ordersService: OrdersService,
              private authGuardService: AuthGuard) {
  }

  ngOnInit() {
    this.currentUserName = this.authGuardService.getCurrentUserLogin();
    this.paginator.pageSize = 5;
    this.paginator.pageIndex = 0;
    this.getOrdersForInput();

  }

  ngAfterViewInit() {
    this.findOnInput();
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.changePage();
  }

  getOrdersForInput() {
    this.ordersService.getOrdersByLoginAndInput(this.currentUserName, this.input.nativeElement.value, this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction).subscribe(
      data => {
        this.orders = data['_embedded'].orderses;
        // this.test = this.orders.forEach(order=>{order['_links'].books})
        this.dataSource = new MatTableDataSource(this.orders);

        this.paginator.length = data['page'].totalElements;

        console.log(this.orders);
      },
      error => {
        console.log(error.error.message);
      }
    );
  }

  changePage() {
    merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.getOrdersForInput())).subscribe();
  }

  findOnInput() {
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.getOrdersForInput();
        })
      )
      .subscribe();
  }

  deleteOrder(id: number) {
    this.ordersService.deleteOrder(id).subscribe(() => this.ngOnInit());
  }

}
