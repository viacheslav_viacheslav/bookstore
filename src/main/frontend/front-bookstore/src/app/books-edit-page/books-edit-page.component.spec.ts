import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BooksEditPageComponent } from './books-edit-page.component';

describe('BooksEditPageComponent', () => {
  let component: BooksEditPageComponent;
  let fixture: ComponentFixture<BooksEditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooksEditPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksEditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
