import { TestBed, inject } from '@angular/core/testing';

import { BookIdTransferService } from './book-id-transfer.service';

describe('BookIdTransferService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookIdTransferService]
    });
  });

  it('should be created', inject([BookIdTransferService], (service: BookIdTransferService) => {
    expect(service).toBeTruthy();
  }));
});
