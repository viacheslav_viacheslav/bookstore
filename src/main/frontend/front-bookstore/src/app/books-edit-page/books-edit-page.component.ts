import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Book} from '../book/book';
import {BookServiceService} from '../books-page/book-service.service';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent} from 'rxjs/observable/fromEvent';
import {merge} from 'rxjs/observable/merge';
import {BookIdTransferService} from './book-id-transfer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-books-edit-page',
  templateUrl: './books-edit-page.component.html',
  styleUrls: ['./books-edit-page.component.css']
})
export class BooksEditPageComponent implements AfterViewInit, OnInit {

  displayedColumns = ['title', 'price', 'photo', 'visible', 'quantity', 'action'];
  private books: Array<Book>;
  public dataSource;
  private bookIdToEdit: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  constructor(private bookService: BookServiceService, private idService: BookIdTransferService, private router: Router) {
  }

  ngOnInit() {
    this.paginator.pageSize = 5;
    this.paginator.pageIndex = 0;
    this.getBooksForInput();
    this.idService.currentId.subscribe(bookId => this.bookIdToEdit = bookId);
  }

  ngAfterViewInit() {
    this.findOnInput();
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.changePage();
  }

  getBooksForInput() {
    this.bookService.getBooksByInput(this.input.nativeElement.value,this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active,
      this.sort.direction).subscribe(
      data => {
        this.books = data['_embedded'].books;
        this.dataSource = new MatTableDataSource(this.books);
        this.paginator.length = data['page'].totalElements;
        console.log(this.books);
      },
      error => {
        console.log(error.error.message);
      }
    );
  }

  changePage() {
    merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.getBooksForInput())).subscribe();
  }

  findOnInput() {
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.getBooksForInput();
        })
      )
      .subscribe();
  }

  deleteBook(id: number) {
    console.log('delete book' + id);
    this.bookService.deleteBook(id).subscribe(() => this.ngOnInit());
  }

  editBook(bookId: number) {
    this.idService.changeId(bookId);
    this.router.navigateByUrl('/layout/bookEdit');
  }

}
