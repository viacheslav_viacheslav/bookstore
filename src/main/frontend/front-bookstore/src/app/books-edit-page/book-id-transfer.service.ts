import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class BookIdTransferService {


  private idSource = new BehaviorSubject(0);
  currentId = this.idSource.asObservable();

  constructor() { }

  changeId(id: number){
    this.idSource.next(id);
  }

}
