import { Component, OnInit } from '@angular/core';
import {AuthGuard} from '../authGuard/auth-guard.service';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent implements OnInit {

  constructor(public authGuardService: AuthGuard) {
  }


  ngOnInit() {
  }

}
