import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {UserService} from '../user.service';
import {AuthGuard} from '../authGuard/auth-guard.service';
import {Location} from '@angular/common';
import {User} from '../users-page/user';


@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {

  user: User=new User();

  genders = ['Male', 'Female'];

  passwordsMatchFlag: boolean = true;

  constructor(public userService: UserService,
              public authGuardService: AuthGuard,
              public location: Location) {
  }

  ngOnInit() {
    if (this.authGuardService.getCurrentUserLogin()){
      this.userService.findByLogin(this.authGuardService.getCurrentUserLogin()).subscribe(data=>{
        this.user=<User>data;
        console.log(this.user.birthday);
        this.user.password=null;
      })
    }
  }

  submit(form: NgForm) {
    if (this.passwordsMatch()) {
      this.userService.save(form).subscribe(data => {
        alert('success');
      });
    }
  }

  passwordsMatch() {
    if (this.user.password != this.user.confirmPassword) {
      this.passwordsMatchFlag=false;
      return false;
    }
    return true;
  }

  cancel(){
    this.location.back();
  }
}
