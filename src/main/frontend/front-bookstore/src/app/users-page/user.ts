export class User {
  userId: string;
  login: string;
  gender: string;
  email: string;
  password: string;
  confirmPassword: string;
  birthday: Date;
}
