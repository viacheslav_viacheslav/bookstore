import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {UserService} from '../user.service';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent} from 'rxjs/observable/fromEvent';
import {merge} from 'rxjs/observable/merge';
import {User} from './user';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.css']
})
export class UsersPageComponent implements AfterViewInit, OnInit {

  displayedColumns = ['login', 'gender', 'email', 'delete'];
  private users: Array<User>;
  public dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.paginator.pageSize = 5;
    this.paginator.pageIndex = 0;
    this.getUsersForInput();
  }

  ngAfterViewInit() {
    this.findOnInput();
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.changePage();
  }

  getUsersForInput() {
    this.userService.getUsersByInput(this.input.nativeElement.value, this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction).subscribe(
      data => {
        this.users = data['_embedded'].users;
        // this.test = this.orders.forEach(order=>{order['_links'].books})
        this.dataSource = new MatTableDataSource(this.users);
        this.paginator.length = data['page'].totalElements;
        console.log(this.users);
      },
      error => {
        console.log(error.error.message);
      }
    );
  }

  changePage() {
    merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.getUsersForInput())).subscribe();
  }

  findOnInput() {
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.getUsersForInput();
        })
      )
      .subscribe();
  }

  deleteUser(id: number) {
    console.log('delete user' + id);
    this.userService.deleteUser(id).subscribe(() => this.ngOnInit());
  }

}
