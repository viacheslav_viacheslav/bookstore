INSERT INTO role (name) VALUES ('ROLE_ADMINISTRATOR'),('ROLE_CUSTOMER');

INSERT INTO user (telephone, email, login, password, gender, birthday,create_date, update_date, blocked)
VALUES (80631354654,'slavon@yandex.ru', 'slavon', '$2a$10$sVOqffM.wDFz7QDiBdFt8O1a/pyVHh3QwaLoSA1wqLQScAp1PbA12','Male','1986-12-24', DEFAULT ,DEFAULT ,0),
  (802345546,'polovinka@yandex.ru', 'polovinka', '$2a$10$Y1yJk58PhF6hEG8RrIVfdetl9GWpAomWhWb35zyowNfRi.BC6tBWu','Female','1900-01-01 00:00:00', DEFAULT ,DEFAULT ,0);

INSERT INTO role_users (users_id, role_id) VALUES (1,1),(2,2);

INSERT INTO category (name) VALUES ('SCIENCE'),('COMEDY'),('THRILLER'),('DETECTIVE'),('ADVENTURES');

INSERT INTO book (title, author, photo_path, price,quantity, category_id, comment, create_date, update_date, price_change_date, visible)
VALUES ('Think Java','Bruce Eckel','Think_java.jpg', 100,2, 1, 'Must read for Java developers',DEFAULT, DEFAULT, NULL, 1),
('Pride and Prejudice','Jane Austen','pride_and_prejudice.jpg', 120,0, 5, 'The nature of this book still lies within the realm of “possible',DEFAULT, DEFAULT, NULL, 1),
('1984','George Orwell','1984.jpg', 200,5,3, 'IGNORANCE IS STRENGTH',DEFAULT, DEFAULT, NULL, 1),
('To Kill a Mockingbird','Harper Lee','To Kill a Mockingbird.jpg', 70,7,4, 'Do not kill anyone',DEFAULT, DEFAULT, NULL, 1),
('The Great Gatsby','F. Scott Fitzgerald','The Great Gatsby.jpg', 150,9,2, 'Here is a novel, glamorous, ironical, compassionate – a marvelous fusion',DEFAULT, DEFAULT, NULL, 1),
('Animal Farm','George Orwell','Animal Farm.jpg', 170,10,1, 'As ferociously fresh as it was more than a half century ago',DEFAULT, DEFAULT, NULL, 1),
('Lord of the Flies','William Golding','Lord of the Flies.jpg', 180,6,3, 'At the dawn of the next world war, a plane crashes on an uncharted island',DEFAULT, DEFAULT, NULL, 1),
('Great Expectations','Charles Dickens','Great Expectations.jpg', 130,7,4, 'In what may be Dickens`s best novel, humble, orphaned Pip is apprenticed to the dirty work of the forge',DEFAULT, DEFAULT, NULL, 1),
('Little Women','Louisa May Alcott','Little Women.jpg', 90,11,1, 'Generations of readers young and old, male and female, have fallen in love with the March',DEFAULT, DEFAULT, NULL, 1),
('Anna Karenina','Leo Tolstoy','Anna Karenina.jpg', 165,5,5, 'Acclaimed by many as the world''s greatest novel, Anna Karenina provides a vast panorama of contemporary life in Russia',DEFAULT, DEFAULT, NULL, 1),
('Brave New World','Aldous Huxley','Brave New World.jpg', 190,13,1, 'Far in the future, the World Controllers have created the ideal society.',DEFAULT, DEFAULT, NULL, 1),
('Wuthering Heights','Emily Brontë','Wuthering Heights.jpg', 140,8,2, 'Wuthering Heights is a wild, passionate story of the intense and almost demonic love between Catherine Earnshaw and Heathcliff',DEFAULT, DEFAULT, NULL, 1),
('Gone with the Wind','Margaret Mitchell','Gone with the Wind.jpg', 200,7,3, 'Gone with the Wind is a novel written by Margaret Mitchell, first published in 1936.',DEFAULT, DEFAULT, NULL, 1),
('Crime and Punishment',' Fyodor Dostoyevsky ','Crime and Punishment.jpg', 70,9,4, 'Poor grannie',DEFAULT, DEFAULT, NULL, 0),
('The Grapes of Wrath',' John Steinbeck','The Grapes of Wrath.jpg', 150,5,2, 'First published in 1939, Steinbeck’s Pulitzer Prize winning epic of the Great Depression',DEFAULT, DEFAULT, NULL, 1),
('The Count of Monte Cristo','Alexandre Dumas','The Count of Monte Cristo.jpg', 170,6,1, 'In 1815 Edmond Dantès, a young and successful merchant sailor',DEFAULT, DEFAULT, NULL, 1),
('One Hundred Years of Solitude',' Gabriel García Márquez','One Hundred Years of Solitude.jpg', 180,9,3, 'The brilliant, bestselling, landmark novel that tells the story of the Buendia family',DEFAULT, DEFAULT, NULL, 1),
('Dracula','Bram Stoker','Dracula.jpg', 130,7,4, 'A true masterwork of storytelling, Dracula has transcended generation, language, and culture',DEFAULT, DEFAULT, NULL, 1),
('The Hobbit','J.R.R. Tolkien','The Hobbit.jpg', 90,7,1, 'In a hole in the ground there lived a hobbit',DEFAULT, DEFAULT, NULL, 1),
('Lolita','Vladimir Nabokov ','Lolita.jpg', 165,9,5, 'Humbert Humbert - scholar, aesthete and romantic - has fallen completely and utterly in love with Lolita Haze',DEFAULT, DEFAULT, NULL, 1),
('Moby-Dick or, The Whale','Herman Melville','Moby-Dick or, The Whale.jpg', 190,10,1, 'It is the horrible texture of a fabric that should be woven of ships cables and hawsers',DEFAULT, DEFAULT, NULL, 1);

INSERT INTO book_category (book_id, category_id) VALUES (1,1);

INSERT INTO orders (user_id, book_id, book_title, comment, create_date, update_date,price)
VALUES (1, 2,'Pride and Prejudice','I bought the book', DEFAULT, DEFAULT, 1200),
(1,  3, '1984', 'I bought the book', DEFAULT, DEFAULT, 34),
(1, 4, 'To Kill a Mockingbird', 'I bought the book', DEFAULT, DEFAULT, 45),
(1, 5, 'The Great Gatsby', 'I bought the book', DEFAULT, DEFAULT, 97),
(1, 6,'Animal Farm', 'I bought the book', DEFAULT, DEFAULT, 86),
(1, 7,'Lord of the Flies',  'I bought the book', DEFAULT, DEFAULT, 99),
(1, 8,'Great Expectations',  'I bought the book', DEFAULT, DEFAULT, 54),
(1, 9, 'Little Women', 'I bought the book', DEFAULT, DEFAULT, 87),
(1, 10,'Anna Karenina', 'I bought the book', DEFAULT, DEFAULT, 56),
(1, 11,'Brave New World', 'I bought the book', DEFAULT, DEFAULT, 56),
(1, 12, 'Wuthering Heights', 'I bought the book', DEFAULT, DEFAULT, 68);

# INSERT INTO book_orders (book_id, orders_id) VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,2),(8,2),(6,3),(2,4),
#   (7,5),(8,6),(3,7),(9,8),(4,9),(7,10),(6,11);