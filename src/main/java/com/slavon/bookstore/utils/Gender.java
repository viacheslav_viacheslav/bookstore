package com.slavon.bookstore.utils;

public enum Gender {

    MALE("Male"), FEMALE("Female");

    public final String gender;

    Gender(String gender) {
        this.gender = gender;
    }
}
