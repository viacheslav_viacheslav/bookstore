package com.slavon.bookstore.service;

import com.slavon.bookstore.entity.Book;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface BookService {
   Book save(Book book);

   String saveFile(MultipartFile file);

   Resource getFile(String filePath);

   Book get(Long id);
}
