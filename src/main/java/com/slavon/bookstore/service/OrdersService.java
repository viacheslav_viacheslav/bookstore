package com.slavon.bookstore.service;

import com.slavon.bookstore.entity.Orders;

public interface OrdersService {

    Orders createOrder(String login, Long bookId);
}
