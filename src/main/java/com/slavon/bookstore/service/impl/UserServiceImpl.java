package com.slavon.bookstore.service.impl;

import com.slavon.bookstore.repository.RoleDao;
import com.slavon.bookstore.repository.UserDao;
import com.slavon.bookstore.service.UserService;
import com.slavon.bookstore.entity.Role;
import com.slavon.bookstore.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService,UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;




    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByLogin(username);
        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), getAuthority(user));

    }

    private List<SimpleGrantedAuthority> getAuthority(User user) {
        return Arrays.asList(new SimpleGrantedAuthority(user.getRole().getName()));
    }

    public User save(User user){
        String encryptedPass = bcryptEncoder.encode(user.getPassword());
        if(user.getRole()==null){
            Role role = roleDao.findRoleByName("ROLE_CUSTOMER");
            user.setRole(role);
        }
        user.setPassword(encryptedPass);
        return userDao.save(user);
    }

}
