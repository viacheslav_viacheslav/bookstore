package com.slavon.bookstore.service.impl;

import com.slavon.bookstore.service.BookService;
import com.slavon.bookstore.repository.BookDao;
import com.slavon.bookstore.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Optional;

@Service(value = "bookService")
public class BookServiceImpl implements BookService {

    private final Path ROOT_LOCATION = Paths.get("src/main/resources/book-images");

    @Autowired
    private BookDao bookDao;

    @Override
    public String saveFile(MultipartFile file) {
        String newFileName= new Date().getTime() + file.getOriginalFilename();
        Path destination = ROOT_LOCATION.resolve(newFileName);
        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, destination);
            return newFileName;
        } catch (IOException ioe) {
            throw new RuntimeException("Image can not be saved", ioe);
        }
    }

    @Override
    public Resource getFile(String filePath) {
        try {
            Path file = ROOT_LOCATION.resolve(filePath);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                return null;
//                throw new RuntimeException("FAIL!");
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed to read the file");
        }
    }

    @Override
    public Book save(Book book) {
        return bookDao.save(book);
    }

    @Override
    public Book get(Long id){
        Optional<Book> optionalBook=bookDao.findById(id);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }
        return null;
    }


}
