package com.slavon.bookstore.service.impl;

import com.slavon.bookstore.repository.BookDao;
import com.slavon.bookstore.service.OrdersService;
import com.slavon.bookstore.repository.OrdersDao;
import com.slavon.bookstore.repository.UserDao;
import com.slavon.bookstore.entity.Book;
import com.slavon.bookstore.entity.Orders;
import com.slavon.bookstore.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service("ordersService")
public class OrdersServiceImpl implements OrdersService{

    @Autowired
    private OrdersDao ordersDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private BookDao bookDao;

    @Override
    @Transactional
    public Orders createOrder(String login, Long bookId) {
        Optional<Book> optionalBook = bookDao.findById(bookId);
        if (!optionalBook.isPresent()){
           throw new RuntimeException("Book is unavailable");
        }
        Book book = optionalBook.get();
        if (book.getQuantity()<1){
            throw new RuntimeException("Book is out of stock");
        }
        book.setQuantity(book.getQuantity()-1);
        User user = userDao.findByLogin(login);
        Orders order = new Orders(user,book);
        return ordersDao.save(order);
    }
}
