package com.slavon.bookstore.service;

import com.slavon.bookstore.entity.User;

public interface UserService {
    User save(User user);
}
