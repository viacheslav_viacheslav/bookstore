package com.slavon.bookstore.projections;

import com.slavon.bookstore.entity.Orders;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;


@Projection(name="orderProjection", types = Orders.class)
public interface OrderProjection {
    Long getId();

    @Value("#{target.book.title}")
    String getBookTitle();

}
