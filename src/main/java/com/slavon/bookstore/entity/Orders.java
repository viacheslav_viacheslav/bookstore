package com.slavon.bookstore.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Orders {

    public Orders() {
    }

    public Orders(User user, Book book){
        this.book=book;
        this.bookTitle=book.getTitle();
        this.user=user;
        this.price=book.getPrice();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Transient
    private Long orderId;

    @ManyToOne
    private User user;


    @ManyToOne
    private Book book;

    private String comment;

    private Date createDate;

    private Date updateDate;

    private String bookTitle;

    private Float price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return id;
    }

    public void setOrderId() {
        this.orderId = this.id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle() {
        this.bookTitle = book.getTitle();
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
