package com.slavon.bookstore.controller;

import com.slavon.bookstore.service.UserService;
import com.slavon.bookstore.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@CrossOrigin(origins = "http://localhost:4200")
@RepositoryRestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users/save")
    public ResponseEntity<Resource<User>> save(@RequestBody User user) {
        User persistedUser = userService.save(user);
        Resource<User> resource = new Resource<>(persistedUser);
        resource.add(linkTo(methodOn(UserController.class).save(user)).withSelfRel());
        return ResponseEntity.ok(resource);
    }
}
