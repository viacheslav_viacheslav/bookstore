package com.slavon.bookstore.controller;

import com.slavon.bookstore.service.OrdersService;
import com.slavon.bookstore.entity.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class OrderController {

    private OrdersService ordersService;

    @Autowired
    public OrderController(OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/orderses/add")
    ResponseEntity<Resource<Orders>> addOrder(@Param("bookId") Long bookId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Orders order = ordersService.createOrder(auth.getName(), bookId);
        System.out.println(auth.getName());
        Resource<Orders> resource = new Resource<>(order);
        resource.add(linkTo(methodOn(OrderController.class).addOrder(bookId)).withSelfRel());
        return ResponseEntity.ok(resource);
    }

}
