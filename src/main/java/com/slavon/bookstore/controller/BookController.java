package com.slavon.bookstore.controller;

import com.slavon.bookstore.service.BookService;
import com.slavon.bookstore.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/books/add")
    public ResponseEntity<Resource<String>> save(@RequestParam("name") String name,
                                                 @RequestParam(value = "file", required = false) MultipartFile file,
                                                 @RequestParam(value = "price", required = false) String price,
                                                 @RequestParam(value = "visible", required = false) String visible,
                                                 @RequestParam(value = "quantity", required = false) String quantity) {
        try {
            Book book = new Book();
            if (name != null) {
                book.setTitle(name);
            }
            if (file != null) {
                book.setPhotoPath(bookService.saveFile(file));
            }
            if (price != null) {
                book.setPrice(Float.valueOf(price));
            }
            if (visible != null) {
                book.setVisible(Boolean.valueOf(visible));
            }
            if (quantity !=null){
                book.setQuantity(Integer.valueOf(quantity));
            }
            bookService.save(book);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/books/update")
    public ResponseEntity<Resource<String>> update(@RequestParam("bookId") String bookId,
                                                   @RequestParam(value = "name", required = false) String name,
                                                   @RequestParam(value = "file", required = false) MultipartFile file,
                                                   @RequestParam(value = "price", required = false) String price,
                                                   @RequestParam(value = "visible", required = false) String visible,
                                                   @RequestParam(value = "quantity", required = false) String quantity) {
        try {
            Book book = bookService.get(Long.valueOf(bookId));
            if (name != null) {
                book.setTitle(name);
            }
            if (file != null) {
                book.setPhotoPath(bookService.saveFile(file));
            }
            if (price != null) {
                book.setPrice(Float.valueOf(price));
            }
            if (visible != null) {
                book.setVisible(Boolean.valueOf(visible));
            }
            if (quantity !=null){
                book.setQuantity(Integer.valueOf(quantity));
            }
            bookService.save(book);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }



    @RequestMapping(method = RequestMethod.GET, value = "/books/files/{filename:.+}")
    public ResponseEntity<org.springframework.core.io.Resource> getFile(@PathVariable String filename) {
        org.springframework.core.io.Resource file = bookService.getFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + file.getFilename() + "\"").contentType(MediaType.IMAGE_JPEG)
                .body(file);
    }

}
