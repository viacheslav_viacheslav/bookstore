package com.slavon.bookstore.repository;

import com.slavon.bookstore.entity.Orders;
import com.slavon.bookstore.projections.OrderProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
@RepositoryRestResource
public interface OrdersDao extends PagingAndSortingRepository<Orders,Long>{

    @RestResource(path="login")
    Page findAllByUserLogin(@Param("login") String login, Pageable p);

    @RestResource(path="findForLogin")
    Page findAllByUserLoginAndBookTitleContaining(@Param("login")String login, @Param("bookTitle") String price, Pageable p);
}
