package com.slavon.bookstore.repository;

import com.slavon.bookstore.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
@RepositoryRestResource
public interface UserDao extends PagingAndSortingRepository<User,Long>{

    @RestResource(path="findByLogin")
    User findByLogin(@Param("login")String login);

    @RestResource(path="findByInput")
    Page findAllByLoginContaining(@Param("login") String price, Pageable p);

}
