package com.slavon.bookstore.repository;

import com.slavon.bookstore.entity.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface RoleDao extends PagingAndSortingRepository<Role,Long>{

    Role findRoleByName(String name);
}
