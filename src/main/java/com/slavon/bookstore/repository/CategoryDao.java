package com.slavon.bookstore.repository;

import com.slavon.bookstore.entity.Category;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
interface CategoryDao extends PagingAndSortingRepository<Category,Long>{
}
