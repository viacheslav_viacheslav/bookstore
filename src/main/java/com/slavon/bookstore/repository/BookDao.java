package com.slavon.bookstore.repository;

import com.slavon.bookstore.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;


@CrossOrigin(origins = "http://localhost:4200")
@RepositoryRestResource
public interface BookDao extends PagingAndSortingRepository<Book,Long> {
    @RestResource(path="visible")
    Page findByVisibleTrue(Pageable p);

    @RestResource(path="findByInput")
    Page findAllByTitleContaining(@Param("bookTitle") String price, Pageable p);
}
